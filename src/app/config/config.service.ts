import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
  })
export class ConfigService {
  constructor(private http: HttpClient) { }

  getListOfFiles(): Observable<any> {
    return this.http.get('http://localhost:8080/getListOfValidFile').pipe(res => res);
  }

  getExperiment(name): Observable<any> {
    return this.http.get('http://localhost:8080/getExperiment?file='+name).pipe(res => res);
  }
}