import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/config/config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  title = 'angular-micareo';
  names : any;
  constructor(private configService: ConfigService,
    private router: Router) { }

  ngOnInit() {
    this.configService.getListOfFiles().subscribe(result => {
      this.names = result;
    })
  }

  clickButton () {
    this.configService.getListOfFiles().subscribe(result => {
      this.names = result;
    })
  }

  getInfo(x) {
    console.log(x);
    var splitted = x.split(".", 2); 
    this.router.navigate(['/experiment'], { queryParams: { name: x } });
  }
}
