import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ConfigService } from 'src/app/config/config.service';

@Component({
  selector: 'app-experiment',
  templateUrl: './experiment.component.html',
  styleUrls: ['./experiment.component.scss']
})
export class ExperimentComponent implements OnInit {
  title: any;
  name:any;
  cells;
  constructor(
    private route: ActivatedRoute,
    private configService: ConfigService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.name = params['name'];
      console.log("this.name");
      console.log(this.name);
      this.configService.getExperiment(this.name).subscribe(result => {
        this.cells = result;
      })
    });
  }

  back() {

  }

}
