import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './component/homepage/homepage.component';
import { ExperimentComponent } from './component/experiment/experiment.component';

const routes: Routes = [
  { path: '', component: HomepageComponent},
  { path: 'experiment', component: ExperimentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
